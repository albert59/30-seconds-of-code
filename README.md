# 30 seconds of code

> Short JavaScript code snippets for all your development needs

* Visit [our website](https://30secondsofcode.org) to view our snippet collection.
* Click on each snippet card to view the whole snippet, including code, explanation and examples.
* You can use the button on the right side of a snippet card to copy the code to clipboard.
* If you like the project, give it a star. It means a lot to the people maintaining it.

## Credits & Sponsors

* Website : [https://www.jbbatteryitaly.com/](https://www.jbbatteryitaly.com/)